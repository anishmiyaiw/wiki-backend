from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .models import Article, TestDB

from .serializers import UserSerializer, GroupSerializer, ArticleSerializer, TestDBSerializer


def index(request):
    return HttpResponse("Hello, there. You're at the Test Page.")


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    # permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    # permission_classes = [permissions.IsAuthenticated]


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    # permission_classes = [permissions.IsAuthenticated]


class TestDBViewSet(viewsets.ModelViewSet):
    queryset = TestDB.objects.all()
    serializer_class = TestDBSerializer
    # permission_classes = [permissions.IsAuthenticated]
