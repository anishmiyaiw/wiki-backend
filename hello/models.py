from django.db import models
import uuid
from django.contrib.auth.models import User


class Article(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=120, unique=False)
    description = models.TextField()
    gps = models.CharField(max_length=120, unique=True, )
    date = models.DateField(auto_now_add=True, blank=True)
    status = models.BooleanField()

    def __str__(self):
        return self.title


class TestDB(models.Model):
    title = models.CharField(max_length=120, unique=False)
    description = models.TextField()
    gps = models.CharField(max_length=120, unique=True, )
    date = models.DateField(auto_now_add=True, blank=True)
    status = models.BooleanField()

    def __str__(self):
        return self.title
